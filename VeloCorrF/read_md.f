C...............................................................
C
      subroutine read_md(ii1,is1,il1,nat,varcel,veloc,nstep)        
C
C     reads from ii1 (MD file), makes some consistency tests,
C     finds out whether the records are for fixed cell or variable cell,
C     copies the velocities into direct-access is1 file,
C     returns the number of MD steps.
C
C     MD file is written in iomd.f as follows (for formt = F ):
C       write(iupos) istep, xa(1..3,1..nat), va(1..3,1..nat)
C       if ( varcel ) write(iupos) cell(1..3,1..3), vcell(1..3,1..3)
C
      implicit none
      integer ii1,is1,il1,nat,nstep,istep,istep1,istep2,ii
      logical varcel
      double precision dummy,b2ang,veloc(3*nat)

      parameter (b2ang=0.52917721092)  !  Bohr to Angstroem

      rewind (ii1)
C --- assume MD was written with fixed cell...
      read (ii1,err=801,end=107) istep1,(dummy,ii=1,6*nat)
      read (ii1,err=102,end=107) istep2,(dummy,ii=1,6*nat)
      if (istep2.eq.istep1+1) then
        varcel = .false.
        write (6,*) ' .MD seems to be written without variable cell'
        goto 101
      endif
C --- open LOG file for reporting suspicious jumps of coordinates
      open(il1,file='MD.LOG',form="formatted",status="unknown")

  102 rewind (ii1)
C --- if it fails, assume MD was written with variable cell...
      read (ii1,err=801,end=107) istep1,(dummy,ii=1,6*nat)
      read (ii1,err=803,end=107) (dummy,ii=1,18)
      read (ii1,err=802,end=107) istep2,(dummy,ii=1,6*nat)
      if (istep2.eq.istep1+1) then
        write (6,*) ' .MD seems to be written with variable cell...'
        write (6,*) '  This is not the standard option for the MD,'
        write (6,*) '  make sure you know what you are doing !'
        varcel = .true.
        goto 101
      endif
      write (6,*) ' Fail to identify variable cell in the .MD '
      stop
  101 continue
      nstep = 0
      rewind (ii1)
C --- read through input file, to get the total number of MD steps.
  104 continue
      nstep = nstep + 1
C ... variable format ( formt=F in subr. iomd):
      read (ii1,err=804,end=107) istep,(dummy,ii=1,3*nat),veloc
!     veloc in MD are in (Bohr/fs); write to is1 and il2 in (Ang/fs) :
      write (is1,rec=nstep) veloc*b2ang
! --- Debugging :
      write (il1,'(3f16.8)') veloc*b2ang
      write(il1,'(1x)')

!     write (19,*) veloc*b2ang
!     write(19,'(1x)')

      if (varcel) read (ii1,err=805,end=108) (dummy,ii=1,18)
      if (mod(nstep,10).eq.0) write (6,*) ' MD record No. ',nstep,
     .                        ',  istep=',istep
      goto 104
  108 continue
C --- irregular end of records in MD file: 
      write (6,*) ' Uncomplete record in MD step No.',nstep
      nstep = nstep - 1  !  No. of full records
      if (nstep.gt.0) then
        write (6,*) ' Keep ',nstep,' records.'
        return
      else
        write (6,*) ' Check the MD file; bye'
        stop
      endif
  107 continue
C --- regular end of records in MD or ANI file: 
      nstep = nstep - 1  !  Attempt to read record Nr. nstep failed
      close (il1)
      if (nstep.gt.0) then
        write (6,*) '  Cleanly read in ',nstep,'  MD steps'
        return
      else
        write (6,*) ' End of record in the firt step: MD file empty?'
        stop
      endif
  801 continue
      write (6,*) ' Error reading coordinates ',
     .            ' for MD step No. 1,  istep1=',istep1
      stop
  802 continue
      write (6,*) ' Error reading coordinates ',
     .            ' for MD step No. 1,  istep2=',istep2
      stop
  803 continue
      write (6,*) ' Error reading variable cell ',
     .            ' for MD step No. 1'
      stop
  804 continue
      write (6,*) ' Error reading coordinates ',
     .            ' for MD step No. ',nstep,'  istep=',istep
      stop
  805 continue
      write (6,*) ' Error reading variable cell ',
     .            ' for MD step No. ',nstep,'  istep=',istep
      stop
      
      end 
