!...............................................................
!
      subroutine read_ani(ii3,is1,il1,nat,tstep,nstep)        
!
!     reads from ii3 (ANI file), makes some consistency tests,
!     calculates velocities from coordinates (present step and step-2)
!     and writes then onto direct-access binary file is1,
!     returns the number of MD steps. io3 is the LOG file
!     for reporting irregularities.
!     tstep :  time step of MD run passed from outside
!     (for correcty calculating the velocities)
!     nstep :  number of records returned in the is1 file
!              (those with velocities, 
!               i.e. number of coord. records in ii3 minus 2 )
!
!     Units of velocities written into is1 are Ang/fs
!
!     ANI file is written in pixmol.f,
!     having the structure of record (MD step) as follows:
!       - Number of atoms;
!       - Empty line;
!       - Line by line, chem. labelof atom and its three cartesian coords
!         (in Angstroem)
!
      implicit none
      integer ii3,is1,il1,nat,iat,nstep,ii,iline,nat0,ialloc
      character*2 lab
      double precision tstep
      double precision, allocatable :: coor0(:,:),coor1(:,:),coor2(:,:)
      allocate (coor0(3,nat),STAT=ialloc)
      allocate (coor1(3,nat),STAT=ialloc)
      allocate (coor2(3,nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Stop in test_mdcar : fails to allocate space',
     .              ' for ',nat,' atoms.'
        stop
      endif

      rewind (ii3)
      nstep=1
      iline=1
  101 read (ii3,"(i5)",err=801,end=107) nat0  ! -- start reading new record
      if (nat0.ne.nat) then
        write (6,*) ' Error reading line ',iline,': expected nat =',
     ,              nat,' but encountered ',nat0
        stop
      endif
      iline = iline+1
      read (ii3,*,err=801,end=108) 
C --- keep the relative coordinates from the previous two steps :
      if (nstep.gt.2) coor2 = coor1
      if (nstep.gt.1) coor1 = coor0
      do iat=1,nat
        iline = iline+1
        read(ii3,"(a2,2x,3f12.6)",err=802,end=108) 
     .       lab,(coor0(ii,iat),ii=1,3)  !  Coords in ANI are Cartesian Ang
      enddo

! --- Now,  extract the velocities, as (coor0 - coor2)/(2*tstep)
      if (nstep.gt.2) then  ! -- two first records only deliver coordinates
                            !    the velocities are stored from the 3d step on:
         write(il1,301) 0.5 / tstep * (coor0-coor2)
         write(il1,'(1x)') 
! ---    This was debug output...
!        and below - the genuine writing into is1 file,
!        skippping two first records of coordinates :
         write (is1,rec=nstep-2) 0.5 / tstep * (coor0-coor2)
! ---    This was a two-step calculation (hopefully eliminating
!        the 1st order error in calculating the derivative);
!        otherwise the "naive" one-step calculation would be as follows:
!        write (is1,rec=nstep-2) 1.0 / tstep * (coor0-coor1)
      endif
  301 format(3f16.8)

C     arrived here if a block is fully read in; count this MD step:
      nstep = nstep+1
      goto 101

C --- regular end of records in the ANI file: 
  107 nstep = nstep - 1   !   Compensate for ++1 added before goto 101
      close (il1)
      if (nstep.gt.0) then
        write (6,*) '  Cleanly read in ',nstep,'  MD steps'
        nstep = nstep - 2   !  Number of velocity records in the is1 file,
!                              (the Nr of coordinate records in in2) - 2
        return     ! --- regular return
      else
        write (6,*) ' End of record in the firt step: MD file empty?'
        stop
      endif

  801 write (6,*) ' Error reading line Nr. ',iline,' :'
      write (6,*) ' Header of block for MDstep Nr ',nstep
      stop
  802 write (6,*) ' Error reading line Nr. ',iline,' :'
      write (6,*) ' Atom coord. block for MDstep Nr ',nstep
      stop
  108 write (6,*) ' Premature end of ANI : MD block No.',
     .              nstep,' is not full. '
      stop

      end 
