!...............................................................
!
      subroutine read_mdcar(ii2,is1,il1,nat,ndif,tstep,nstep)        
!
!     reads from ii2 (MD_CAR file), makes some consistency tests,
!     calculates velocities from coordinates (present step and step-2)
!     and writes then onto direct-access binary file is1,
!     returns the number of MD steps. io3 is the LOG file
!     for reporting irregularities.
!     nat   :  number of atoms in XV file (to check against)
!     ndif  :  number of different species
!     tstep :  time step of MD run passed from outside
!     (for correcty calculating the velocities)
!     nstep :  number of records returned in the is1 file
!              (those with velocities, 
!               i.e. number of coord. records in ii2 minus 2 )
!
!     Units of velocities written into is1 are Ang/fs
!
!     MD_CAR file is written in md_out.F90,
!     having the structure of record (MD step) as follows:
!       - two title lines ( ---??--- //  1.0 );
!       - three lines with cell parameters in Ang;
!       - number of atoms;
!       - the word "Direct";
!       - line by line, relative cord. of atoms (in units of lattice constants)
!
      implicit none
      integer ii2,is1,il1,nat,iat,nstep,ii,is,nerr,iline,nat0,ialloc,
     .        ndif,idif
      character*3 lab1
      character*6 lab2
      real num1
      double precision dummy,tstep,cell(3,3),
     .                 cell0(3,3),cell1(3,3),cell2(3,3)
      integer,  allocatable :: natm(:)
      double precision,  allocatable :: veloc(:,:),
     .       coor0(:,:),coor1(:,:),coor2(:,:),coorp(:,:),coorm(:,:) 
      allocate (natm(ndif),STAT=ialloc)
      allocate (coor0(3,nat),STAT=ialloc)
      allocate (coor1(3,nat),STAT=ialloc)
      allocate (coor2(3,nat),STAT=ialloc)
      allocate (coorm(3,nat),STAT=ialloc)
      allocate (coorp(3,nat),STAT=ialloc)
      allocate (veloc(3,nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Stop in test_mdcar : fails to allocate space',
     .              ' for ',nat,' atoms.'
        stop
      endif

C --- open LOG file for reporting suspicious jumps of coordinates
      open(il1,file='MD_CAR.LOG',form="formatted",status="unknown")
      nerr = 0

      rewind (ii2)
      nstep=1
      iline=1
  101 read (ii2,"(a3)",err=801,end=107) lab1  ! -- start reading new record
      if (lab1.ne.'---') then
        write (6,*) ' Error reading line ',iline,': expected ---',
     ,              ' but encountered ',lab1
        stop
      endif
      iline = iline+1
      read (ii2,"(f10.1)",err=801,end=108) num1  ! 2d line of block must be 1.0
      if (num1.ne.1.0) then
        write (6,*) ' Error reading line ',iline,': expected    1.0',
     ,              ' but encountered ',num1
        stop
      endif
C --- keep the cell vectors from the previous two steps :
      if (nstep.gt.2) cell2 = cell1
      if (nstep.gt.1) cell1 = cell0
      do ii=1,3
        iline = iline+1
        read(ii2,"(3f16.9)",err=801,end=108) cell0(:,ii)
      enddo
      iline = iline+1
      read(ii2,"(30i6)",err=801,end=108) natm(:) ! Nr atoms of diff. species
      nat0 = 0
      do idif = 1,ndif
        nat0 = nat0 + natm(idif)  !  recover the total Nr of atoms,
      enddo
      if (nat0.ne.nat) then
        write (6,*) ' Error reading line ',iline,': expected nat =',
     ,              nat,' but encountered ',nat0
        stop
      endif
      iline = iline+1
      read (ii2,"(a)",err=801,end=108) lab2
      if (lab2.ne.'Direct') then
        write (6,*) ' Error reading line ',iline,': expected "Direct"',
     ,              ' but encountered ',lab2
        stop
      endif
C --- keep the relative coordinates from the previous two steps :
      if (nstep.gt.2) coor2 = coor1
      if (nstep.gt.1) coor1 = coor0
      do iat=1,nat
        iline = iline+1
        read(ii2,"(3f16.9)",err=802,end=108) (coor0(ii,iat),ii=1,3)
      enddo
C --- Now,  extract the velocities, as (coor0 - coor2)/(2*tstep)
C     The difficulties: coordinates are relative (for this we've stored
C     the lattice constants as well); moreover, in a periodic system
C     the atoms' coordinates may undergo a translation to return them
C     within [0,1]. We assume that the atom may be "reasonably" displaced, 
C     on each MD step, by, at most, +/-0.4 of any lattice constant. 
C     The jumps by [-0.4...0.4] are accepted as such,
C     those by [-1...-0.6] or [0.6...1.0] are silently corrected
C     by adding / subtracting a translation; those within +/-[0.4...0.6]
C     are considered dangerous and are reported.
C     The coordinates from the step -1 are taken as reference,
C     those at steps 0 and -2 are corrected before calculating velocities.
C     The corrected (or not) relatice voordinates are put into
C     coorm (step -2) and coorp (step 0)
C
      if (nstep.gt.2) then  ! --- two first records only deliver coordinates
        do iat=1,nat 
        do ii=1,3    ! --- check throughout all relative coordinates...
          if (  abs(coor0(ii,iat)-coor1(ii,iat)) .lt. 0.4) then
            coorp(ii,iat)=coor0(ii,iat) 
          elseif ( (coor0(ii,iat)-coor1(ii,iat)) .gt. 0.6) then
            coorp(ii,iat)=coor0(ii,iat)-1.0
          elseif ( (coor1(ii,iat)-coor0(ii,iat)) .gt. 0.6) then
            coorp(ii,iat)=coor0(ii,iat)+1.0
          else ! accept, but report irregularity. The velocity will be junk
            coorp(ii,iat)=coor0(ii,iat)
            write (il1,201) iat,nstep,(coor0(is,iat),is=1,3),
     .                              (coor1(is,iat),is=1,3)
            nerr = nerr+1
          endif
          if (  abs(coor2(ii,iat)-coor1(ii,iat)) .lt. 0.4) then
            coorm(ii,iat)=coor2(ii,iat) 
          elseif ( (coor2(ii,iat)-coor1(ii,iat)) .gt. 0.6) then
            coorm(ii,iat)=coor2(ii,iat)-1.0
          elseif ( (coor1(ii,iat)-coor2(ii,iat)) .gt. 0.6) then
            coorm(ii,iat)=coor2(ii,iat)+1.0
          else ! accept... This jump must have been reported in prev. MD step
            coorm(ii,iat)=coor2(ii,iat)
          endif
        enddo
        enddo
! ---   extract velocities from cartesian coordinates and write down:
!       (units are: Ang / fs )
        write(il1,301) 0.5 / tstep *
     .    (matmul(cell0,coorp)-matmul(cell2,coorm))
        write(il1,'(1x)') 
! ---   This was debug output...
!       and below - the genuine writing into is1 file,
!       skippping two first records of coordinates :
        write (is1,rec=nstep-2) 0.5 / tstep *
     .    (matmul(cell0,coorp)-matmul(cell2,coorm))
      endif
  301 format(3f16.8)

C     arrived here if a block is fully read in; count this MD step:
      nstep = nstep+1
      goto 101

C --- regular end of records in MD_CAR file: 
  107 nstep = nstep - 1   !   Compensate for ++1 added before goto 101
      close (il1)
      if (nstep.gt.0) then
        write (6,*) '  Cleanly read in ',nstep,'  MD steps'
        if (nerr.gt.0) write (6,*) ' There were suspicious',
     .     ' coordinate jumps, check the LOG file' 
        nstep = nstep - 2   !  Number of velocity records in the is1 file,
!                              (the Nr of coordinate records in in2) - 2
        return     ! --- regular return
      else
        write (6,*) ' End of record in the firt step: MD file empty?'
        stop
      endif

  801 write (6,*) ' Error reading line Nr. ',iline,' :'
      write (6,*) ' Header of block for MDstep Nr ',nstep
      stop
  802 write (6,*) ' Error reading line Nr. ',iline,' :'
      write (6,*) ' Atom coord. block for MDstep Nr ',nstep
      stop
  108 write (6,*) ' Premature end of MD_CAR : MD block No.',
     .              nstep,' is not full. '
      stop

  201 format(' Coords jump for atom',i5,'  at step',i5,
     .       ' : to (',3f10.6,' ) from (',3f10.6,' )')
      
      end 
