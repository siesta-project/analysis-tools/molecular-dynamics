C
C    velcf,  a script to calculate the velocity correlation function
C            and its Fourier transform from molecular dynamics files 
C            (MD, MD_CAR, or ANI) written in SIESTA 
C
C       Written by Andrei Postnikov, May 2007   
C       Modifications to include MD_CAR and ANI, Dec 2014   
C       andrei.postnikov@univ-lorraine.fr
C
      program velcf
      implicit none
      integer ii0,ii1,ii2,ii3,io1,io2,is1,il1,
     .        iat,nat,na,ialloc,ii,jj,mdmod,
     .        nstep,mdfirst,mdlast,mdstep,mdlim,
     .        ncorr,imd,jmd,jmdmax,kmd,ndif,idif
      parameter (ii0=11,ii1=12,ii2=13,ii3=14,io1=15,io2=16,
     .           is1=17,il1=18)

      character inpfil*60,outfi1*60,outfi2*60,syslab*30,suffix*6
      logical varcel,found
      double precision b2ang,cc_bohr(3,3),cc_ang(3,3),cc_velo(3,3),
     .                 dummy,tstep,vcf,vcfc,vcfs,wfac,omega1,omega2
      parameter (b2ang=0.529177)   !  Bohr to Angstroem
      double precision,  allocatable :: veloc1(:),veloc2(:),vct(:)
      integer,           allocatable :: nz(:),ityp(:),ispec(:),nct(:)
      external read_md,read_mdcar,read_ani,opnout
!
!     string manipulation functions in Fortran used below:
!     len_trimd(string): returns the length of string 
!                       without trailing blank characters,
!     trim(string)    : returns the string with railing blanks removed
      
      write (6,701,advance="no")
      read (5,*) syslab
!     inpfil = syslab(1:len_trim(syslab))//'.XV'
      inpfil = trim(syslab)//'.XV'
      open (ii0,file=inpfil,form='formatted',status='old',err=801)
      write (6,*) 'Found and opened: ',inpfil
! --  read in translation vectors, convert into Ang:
      do ii=1,3
        read  (ii0,702,end=803,err=803)  (cc_bohr(jj,ii),jj=1,3)
      enddo
      cc_ang = cc_bohr*b2ang
      read  (ii0,*,end=804,err=804)  nat
! ---   allocate space for velocities from two records, for scalar product:
      allocate (veloc1(3*nat),STAT=ialloc)
      allocate (veloc2(3*nat),STAT=ialloc)
      allocate (nz(nat),STAT=ialloc)
      allocate (ityp(nat),STAT=ialloc)
      allocate (ispec(nat),STAT=ialloc)
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',nat,' atoms.'
        stop
      endif
! --- find out the number of different types (needed for reading MD_CAR only;
!     how many atoms of each type is irrelevant - we'll check the sum though)
      ndif = 0
      do iat=1,nat
        read (ii0,703,end=805,err=805) ityp(iat), nz(iat), 
     .       (dummy,ii=1,3)   !  coordinates are of no interest here
        found = .false.
        if (ndif.gt.0) then
          do idif = 1,ndif
            if (ityp(iat).eq.ispec(idif)) then
              found = .true.
              exit
            endif
          enddo
        endif
        if (.not.found) then
          ndif = ndif + 1
          ispec(ndif) = ityp(iat)
        endif
      enddo
      close (ii0)
      write (6,*) ' with ',nat,'  atoms of ',ndif,' different species'
C --- finished with .XV
C     Ask about simulation time step 
C     - strangely, it is not stored along with the MD history files...
      write (6,709,advance="no") 
      read (5,*) tstep

C --- open direct-access file for velocities (of all atoms);
C     No. of records is number of MD steps retained. The idea is
C     that we may have large number of atoms and huge number of MD steps,
C     so that it isn't practical to keep all in memory. At the same time,
C     calculating correlations demands to address (many times) data from 
C     diferent MD steps. Moreover, there is diversity of formats
C     in which MD data are stored. So different routines are used
C     to extract data from different inputs and write into a direct-access file
C     in the same format, whereupon the calculation of correlations
C     proceeds in the same way.
      open (is1,access='direct',status='scratch',form='unformatted',
     .      recl=sizeof(veloc1),err=807)   

  103 write (6,704,advance="no")
      read (5,*) suffix
      inpfil = trim(syslab)//'.'//trim(suffix)
      if (trim(suffix).eq.'MD') then
        mdmod = 1
        open (ii1,file=inpfil,form='unformatted',status='old',err=806)
        write (6,*) 'Found and opened: ',inpfil
        call read_md(ii1,is1,il1,nat,varcel,veloc1,nstep)
        write (6,*) nstep,' MD records (of velocities) extracted'
        close (ii1)
      elseif (trim(suffix).eq.'MD_CAR') then
        mdmod = 2
        open (ii2,file=inpfil,form='formatted',status='old',err=801)
        write (6,*) 'Found and opened: ',inpfil
        call read_mdcar(ii2,is1,il1,nat,ndif,tstep,nstep)
        write (6,*) nstep,' MD records (of velocities) generated'
        close (ii2)
      elseif (trim(suffix).eq.'ANI') then
        mdmod = 3
        open (ii3,file=inpfil,form='formatted',status='old',err=801)
        write (6,*) 'Found and opened: ',inpfil
        call read_ani(ii3,is1,il1,nat,tstep,nstep)
        close (ii3)
      else
        write (6,*) ' Only MD, MD_CAR or ANI allowed, try again:'
        goto 103
      endif
!
! --- select MD steps for correlation analysis from the total data available:
!
  104 write (6,705,advance="no")
      mdfirst = 0
      mdlast  = 0
      read (5,*) mdfirst, mdlast
      if (mdfirst.eq.0)  mdfirst = 1
      if ( (mdlast.le.0).or.(mdlast.gt.nstep) ) mdlast = nstep
      if (mdfirst.lt.0 .or. 
     .    mdfirst.gt.nstep .or. 
     .    mdlast.lt.mdfirst) then
        write (6,*) ' The numbers must be 0 <= MDfirst <= MDlast <= ',
     .                nstep  
        goto 104
      endif
      mdstep = mdlast-mdfirst+1
      write (6,706) mdfirst,mdlast,mdstep

      write (6,707,advance="no") 
      read (5,*) ncorr
      if ((ncorr.le.0.).or.(ncorr.gt.mdstep-1)) ncorr=mdstep-1
! --- try to allocate memory for velocity correlation function:
      allocate (vct(0:ncorr),STAT=ialloc)  ! will be velocity auticorellation
      allocate (nct(0:ncorr),STAT=ialloc)  ! will be its normalization factor
      if (ialloc.ne.0) then
        write (6,*) ' Fails to allocate space for ',ncorr,' steps.'
        stop
      endif
!     write (6,708) ncorr,mdfirst

! --- Calculation of autocorrelation function
!     following an example from Chapter 6 of the book
!     by M.P.Allen and D.J.Tildesley
!     "Computer Simulation of Liquids", Clarendon, Oxford, 1987
!     ( ISBN 0-19-855645-4 )
!
      vct = 0.d0
      nct = 0
! --- write (6,*) ' Constructing real-space velocity correlation function:')
!
      do imd = mdfirst,mdlast   ! -- loop over sampling times
! ---   extract velocities (in units of Ang/fs) from the step  imd : 
        read (is1,rec=imd) veloc1
! ---   Printout the progress... 
        if (mod(imd,10).eq.0) write (6,*) ' Step ',
     .                        imd,'  of  ',mdlast,' done'
! ---   Find number of forward steps from 'imd' on to trace correlations:
        jmdmax = min(mdlast,imd+ncorr)  ! -- farthest step in correlation
        do jmd = 0,jmdmax-imd   ! -- loop over arguments of correlation function
          read (is1,rec=imd+jmd) veloc2
          vct(jmd) = vct(jmd) + dot_product(veloc1,veloc2)
          nct(jmd) = nct(jmd) + 1
        enddo
      enddo
      close (is1)  ! --- done with MD file
      vct(:) = vct(:) / nct(:)

      outfi1 = trim(syslab)//'.VCT'
      call opnout(io1,outfi1)
      write(io1,901)
      do jmd = 0,ncorr
        write(io1,902) jmd,jmd*tstep,vct(jmd)
      enddo
      close (io1)

! --- open output file for Fourier transformed velocity correlation function:
      outfi2 = trim(syslab)//'.VCF'
      call opnout(io2,outfi2)
      write(io2,903)
! --- make slow Fourier transform (cosine; discrete):
!     frequency factor in the cosine argument:
      wfac = 4.0*atan(1.0)/(ncorr+1)
      do kmd = 0,ncorr
        vcfc = 0.d0
        vcfs = 0.d0
        do jmd = 0,ncorr
          vcfc = vcfc + vct(jmd)*cos(wfac*jmd*kmd)
          vcfs = vcfs + vct(jmd)*sin(wfac*jmd*kmd)
        enddo
        vcfc = vcfc * 2.0/(ncorr+1)
        vcfs = vcfs * 2.0/(ncorr+1)
        vcf = sqrt(vcfc*vcfc + vcfs*vcfs)
!       Frequency in THz:
        omega1 = 500.0/((ncorr+1)*tstep)*kmd
!       Frequency in cm-1:
        omega2 = omega1 * 33.356
        write(io2,904) kmd,omega1,omega2,vcfc,vcfs,vcf
      enddo
      close (io2) 

      deallocate (veloc1,veloc2,nz,ityp,vct,nct)

      stop

  701 format(" Specify  SystemLabel (or 'siesta' if none): ")
  702 format(3x,3f18.9)
  703 format(i3,i6,3f18.9)
  704 format(' Suffix of molecular dynamics file',/
     .       ' (MD or MD_CAR or ANI): ')
  705 format(' You may wish to retain only part of this MD',
     .       ' history for subsequent analysis.',/,
     .       ' Specify two numbers MDfirst, MDlast -',
     .       ' or 0 for any of them as default : ')
  706 format(" OK, I'll keep steps Nr.",i6,'   through 'i6,
     .       ' ( total',i6,' )')   
  707 format(' You may wish to limit the maximal correlation length',/,
     .       ' to be calculated. This can make sense',
     .       ' in very long runs,',/,' where the correlations', 
     .       ' fall down much faster. Give the max. number of steps',/,
     .       ' to cutoff correlations (0 for default --> no cutoff) : ')
  708 format(' Allocated memory for ',i8,' steps',/,
     .       ' of the velocity autocorrelation function,',/,
     .       ' starting from step ',i8,' of the MD run.')
  709 format(' Simulation time step in femtaseconds: ')
  901 format('# Velocity autocorrelation function, time domain', /
     .       '#   step',8x,'time(fs)    function')
  902 format(i8,f14.3,f16.10)
  903 format('# Velocity autocorrelation function, frequency domain', /
     .       '#',11x,'---- frequency ----',4x,17('-'),' function ',
     .       17('-'), / '#   step',6x,'(THz)',6x,'(cm-1)',7x,'cos-FT',
     .       10x,'sin-FT',12x,'abs')
  904 format(i8,f11.3,f12.3,3f16.10)

  801 write (6,*) ' Error opening file ',
     .            trim(inpfil),' as old formatted'
      stop
  803 write (6,*) ' End/Error reading XV for cell vector ',ii
      stop
  804 write (6,*) ' End/Error reading XV for number of atoms line'
      stop
  805 write (6,*) ' End/Error reading XV for atom number ',iat
      stop
  806 write (6,*) ' Error opening file ',
     .            trim(inpfil),' as old unformatted'
  807 write (6,*) ' Error opening scratch file for velocities'
      stop

      end
