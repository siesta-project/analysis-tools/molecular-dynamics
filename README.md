# molecular-dynamics

Assorted utilities to analyze molecular-dynamics runs:

* VeloCorrF: computes the velocity correlation function and its
  Fourier transform from molecular dynamics files (by Andrei Postnikov)
  